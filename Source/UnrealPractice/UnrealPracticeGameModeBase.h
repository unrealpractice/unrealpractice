// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UnrealPracticeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UNREALPRACTICE_API AUnrealPracticeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
